[bits 16]

print_char: ; char in al
    pusha
    mov ah, 0x0e
    int 0x10
    popa
    ret

print_string: ; address of 0-terminated string in bx
    pusha
  print_string_start:
    mov al, [bx]
    cmp al, 0
    je print_string_end
    call print_char
    add bx, 1
    jmp print_string_start

  print_string_end:
    popa
    ret

; print_hex: ; value to print in dx
;     pusha
;     mov cx, 4
;   print_hex_loop:      
;     mov bx, dx 
;     and bx, 1111b    
;     add bx, HEX_SYM    
;     mov al, [bx] ; needed hex symbol loaded in al
;     mov bx, HEX_OUT
;     add bx, 1
;     add bx, cx
;     mov [bx], al ; symbol saved at the right position    
;     shr dx, 4
;     sub cx, 1
;     cmp cx, 0
;     jne print_hex_loop
    
;     mov bx, HEX_OUT
;     call print_string
;     popa
;     ret

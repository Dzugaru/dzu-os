#!/bin/bash

# exit when any command fails
set -e

# DEBUG
# This will "mark" each kernel block to debug kernel loading
# for number in {1..896}
# do
# d=$(printf '%04d' $(($number)))
# dl=${d:2:2}
# dh=${d:0:2}
# echo "$(printf '%08x' $((512 * $number))): $dl$dh" | xxd -r - kernel.bin
# done

#NOTE: need to set 512 mb memory 
qemu-system-i386 -no-reboot -drive file=kernel.bin,format=raw,index=0,media=disk -m 512 # -enable-kvm
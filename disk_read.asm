[bits 16]

; load CX sectors to ES:BX address from drive DL and sector AX

disk_read:
    pusha
    push ax

    ; check LBA extensions
    push bx
    push cx
    mov ah, 0x41
    mov bx, 0x55aa
    int 0x13

    jc no_lba    
    pop cx
    pop bx
    pop ax    

    mov word [LbaNumSec], cx    
    mov word [LbaBufAddr], bx
    mov word [LbaBufSeg], es    
    mov word [LbaStart], ax
    mov si, LbaPacket
    mov ah, 0x42

    ; push dx
    ; mov dx, es
    ; call print_hex
    ; pop dx

    int 0x13

    jc  disk_error  

    ; mov dx, [LbaNumSec]
    ; call print_hex

    popa
    ret
disk_error:
    mov bx, DISK_ERROR_MSG
    call print_string
    jmp halt

no_lba:
    mov bx, NO_LBA_MSG
    call print_string
    jmp halt

DISK_ERROR_MSG: db "ErDisk", 0
NO_LBA_MSG: db "ErLBA", 0

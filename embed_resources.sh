#!/bin/bash

# exit when any command fails
set -e

kernel_current_len=$(wc -c < "kernel.bin")
kernel_total_len=$((833*512))
truncate -s $kernel_total_len kernel.bin
res_size=$(wc -c < "resources.bin")
res_offset=$((kernel_total_len - res_size))
echo "Res offset $res_offset, kernel len $kernel_current_len"
if [ $res_offset -lt $kernel_current_len ]; then
    echo "Not enough space for resources!"
    rm kernel.bin
else
    dd if=resources.bin of=kernel.bin oflag=seek_bytes seek=$res_offset
fi
 
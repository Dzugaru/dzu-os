SHELL := /bin/bash

kernel.bin: boot_sect.touch $(shell find kernel/src/ -name *.rs) kernel/target.json kernel/.cargo/config.toml kernel/Cargo.toml resources.bin
	cd kernel && cargo build
	cp kernel/target/target/debug/kernel kernel.bin
	./embed_resources.sh

boot_sect.touch: boot_sect.o
#	TODO: we need to relink everything, but Cargo doesn't know about that
#	so we'll trigger a recompile instead
	touch kernel/src/main.rs
	touch boot_sect.touch

boot_sect.o: *.asm
	nasm boot_sect.asm -f elf -o boot_sect.o

resources.bin: resources/*
	cd res-packer && cargo run ../resources/ ../resources.bin


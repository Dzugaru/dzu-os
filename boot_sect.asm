; DzuOS boot sector
; ..7C00 - stack (growing downward)
; 7C00..7E00 - loaded bootsector
; 7E00 - loaded continuation

; [org 0x7c00] 
[bits 16] ; VERY important, this is not the default (as you can assume when compiling to bin, not elf with nasm, elf will default to 32 and break everything)

jmp word start

OperatingSystemName db "DzuOS   " ;  8 byte
BytesPerSec         dw 512
SecPerClus          db 1
ReservedSec         dw 1
NumFATs             db 0
RootEntries         dw 0
TotSec              dw 833
MediaType           db 0xF0
FATSize             dw 0
SecPerTrack         dw 0
NumHeads            dw 0
HiddenSec           dd 0
TotSec32            dd 0
DriveNum            db 0
Reserved            db 0
BootSig             db 0x29
VolumeSerialNum     dd 0xDEADC0DE
VolumeLabel         db "DZU OS     "   ; 11 byte
FileSys             db "NONE    "      ;  8 byte

; LBA packet
dw 0 ; must be aligned to 4 bytes

LbaPacket   db 0x10
            db 0x00
LbaNumSec   dw 0x0001
LbaBufAddr  dw 0x7E00
LbaBufSeg   dw 0x0000
LbaStart    dw 0x0000
            dw 0x0000
            dw 0x0000
            dw 0x0000

start:

cli
cld
xor    ax,ax 
mov    es,ax
mov    ss,ax
mov    ds,ax
mov    bp,0x7c00 ; stack
mov    sp,bp 

mov [BOOT_DRIVE], dl

; call print_hex

; mov bx, HELLO_MSG
; call print_string
; mov bx, LINE_BREAK
; call print_string

; load (512 * 16 * 52 (832 sectors)) of kernel to 0x10000 address 
; (this will fill memory almost till 0x80000 (TODO: 63 sectors short))
mov cx, 52
mov ax, 1
mov bx, 0x1000
load_kernel_loop:
    push ax
    push bx
    push cx    

    mov es, bx
    mov bx, 0x0
    mov dl, [BOOT_DRIVE]    
    mov cl, 16 ; load 16 sectors
    call disk_read

    pop cx
    pop bx
    pop ax

    add bx, 0x200
    add ax, 16
    sub cx, 1

    ; debug
    ; mov dx, bx 
    ; call print_hex

    cmp cx, 0
    jne load_kernel_loop



; check loaded data
; mov bx, LINE_BREAK
; call print_string
; mov ax, 0x1800
; mov bx, 0
; load_check:
; push ax
; push bx
; mov ds, ax
; mov dx, [bx]
; mov ax, 0
; mov ds, ax
; call print_hex
; pop bx
; pop ax
; add ax, 0x20
; cmp ax, 0x2000
; ; add bx, 2
; ; cmp bx, 32
; jne load_check

; jmp $

; Switch to 320x200x256 VGA mode
mov ah,0
mov al,0x13
int 0x10

; Switch to protected mode


; load GDT descriptor table
lgdt [gdt_descriptor]

mov eax, cr0   ; To make  the  switch  to  protected  mode, we set
or eax, 0x1    ; the  first  bit of CR0 , a control  register
mov cr0, eax   ; Update  the  control  register

jmp  CODE_SEG:start_protected_mode

halt:
    hlt
    jmp halt

%include "prints.asm"
%include "disk_read.asm"
%include "gdt.asm"

[bits 32]
[extern _start]
%include "prints32.asm"
;%include "a20.asm"

start_protected_mode:
cli
cld
mov ax, DATA_SEG         ; Now in PM, our  old  segments  are  meaningless,
mov ds, ax               ; so we  point  our  segment  registers  to the
mov ss, ax               ; data  selector  we  defined  in our  GDT
mov es, ax
mov fs, ax
mov gs, ax
mov ebp, 0x0fffffff ;NOTE: assume this memory range (over 256 mb) exists
mov esp, ebp 

;call is_A20_on

;mov ebx, HELLO_MSG
;call print_string_pm   

; try draw red pixel at 100x100
; mov eax, 0xa0000
; add eax, 320*100
; add eax, 100
; mov byte [eax], 4


call _start

halt32:
    hlt
    jmp halt32


; Data
BOOT_DRIVE: db 0
LINE_BREAK: db 13, 10, 0
; HELLO_MSG: db 'Hello DzuOS!', 0
; HEX_OUT: db '0x0000 ', 0
; HEX_SYM: db '0123456789abcdef', 0

times  510-($-$$) db 0;

dw 0xaa55
;---------------------------------------------;
;  sets the palette (256 colors)              ;
;                                             ;
; input:  esi = palette.                      ;
; output: none.                               ;
;---------------------------------------------;
set_palette256:
     push     ax
     push     cx
     push     dx

     xor     cx, cx
     .l1:
     mov     dx, 0x03C8
     mov     al, cl               ; color no. = loop no.
     out     dx, al
     inc     dx                  ; port 0x3C9
     mov     al, byte [esi]            ; red
     out     dx, al
     inc     esi
     mov     al, byte [esi]            ; green
     out     dx, al
     inc     esi
     mov     al, byte [esi]            ; blue
     out     dx, al
     inc     esi

     inc     cx
     cmp     cx, 256
     jl     .l1

     pop     dx
     pop     cx
     pop     ax
     ret


;---------------------------------------------;
;  sets the palette (16 colors)               ;
;                                             ;
; input:  esi = pointer to palette.           ;
; output: none.                               ;
;---------------------------------------------;
set_palette16:
     push     ax
     push     cx
     push     dx

     xor     cx, cx
     .l1:
     mov     dx, 0x3DA
     in     al, dx
     mov     al, cl               ; color no.
     mov     dx, 0x3C0
     out     dx, al
     inc     dx                  ; port 0x3C1
     in     al, dx
     mov     dx, 0x3C8
     out     dx, al

     inc     dx                  ; port 0x3C9
     mov     al, byte [esi]            ; red
     out     dx, al
     inc     esi
     mov     al, byte [esi]            ; green
     out     dx, al
     inc     esi
     mov     al, byte [esi]            ; blue
     out     dx, al
     inc     esi

     inc     cx
     cmp     cx, 16
     jl     .l1

     mov     dx, 0x3DA
     in     al, dx
     mov     al, 0x20
     mov     dx, 0x3C0
     out     dx, al

     pop     dx
     pop     cx
     pop     ax
     ret
+ Craft .lock() that automatically disables interrupts 
- Embed data into kernel (by writing bash script that checks for free space and writes 
resources folder contents in some simple "filesystem" at the end of kernel.bin)
- Convenience func to inspect memory
- lazy_static not needed? just use Mutex?
- Write spin Mutex (and check other interesting crates too) ourselves
- Fix vs code rust extension to compile kernel code
- Start to write tests
- Serial port support for logging inside QEMU
- Make everything usize where possible!

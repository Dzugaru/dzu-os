VIDEO_MEMORY  equ 0xb8000

[bits 32]

; prints a null-terminated  string  pointed  to by EBX
print_string_pm:
    pusha
    mov edx, VIDEO_MEMORY   
print_string_pm_loop:
    mov al, [ebx]         
    cmp al, 0          
    je print_string_pm_done            
    mov ah, 0x0f
    mov [edx], ax      
    add ebx, 1         
    add edx, 2         
    jmp  print_string_pm_loop   ; loop  around  to  print  the  next  char.
print_string_pm_done:
    popa
    ret                  
#[inline]
pub fn outb(port: u16, data: u8) {
    unsafe { asm!("out dx,al", in("al") data, in("dx") port, options(nostack, nomem)); }
}

#[inline]
pub fn inb(port: u16) -> u8 {
    let data: u8;
    unsafe { asm!("in al,dx", out("al") data, in("dx") port, options(nostack, nomem)); }
    data
}

#[inline]
pub fn outw(port: u16, data: u16) {
    unsafe { asm!("out dx,ax", in("ax") data, in("dx") port, options(nostack, nomem)); }
}

#[inline]
pub fn inw(port: u16) -> u16 {
    let data: u16;
    unsafe { asm!("in ax,dx", out("ax") data, in("dx") port, options(nostack, nomem)); }
    data
}

#[inline]
pub fn outl(port: u16, data: u32) {
    unsafe { asm!("out dx,eax", in("eax") data, in("dx") port, options(nostack, nomem)); }
}

#[inline]
pub fn inl(port: u16) -> u32 {
    let data: u32;
    unsafe { asm!("in eax,dx", out("eax") data, in("dx") port, options(nostack, nomem)); }
    data
}

#[inline]
// Sometimes we need to add a delay between writes, especially on
// older motherboards.  But we don't necessarily have any kind of
// timers yet, because most of them require interrupts.  Various
// older versions of Linux and other PC operating systems have
// worked around this by writing garbage data to port 0x80, which
// allegedly takes long enough to make everything work on most
// hardware. Port 0x80 is used for 'checkpoints' during POST.
pub fn io_wait() {
    unsafe { asm!("mov al,0; out 0x80,al", options(nostack, nomem)); }
}

#[inline]
pub fn are_interrupts_enabled() -> bool {
    let flags: u16;
    unsafe { asm!("pushf",
                  "mov ax, [esp]",
                  "add esp,2",
                  out("ax") flags); } 
    return flags & 0x200 > 0;
}

//Enable interrupts
#[inline]
pub fn sti() {
    unsafe { asm!("sti", options(nostack, nomem)); }
}

//Disable interrupts
#[inline]
pub fn cli() {
    unsafe { asm!("cli", options(nostack, nomem)); }
}

//Hlt
#[inline]
pub fn hlt() {
    unsafe { asm!("hlt", options(nostack, nomem)); }
}

//Enable paging
#[inline]
pub fn enable_paging(page_dir_addr: u32) {
    unsafe {
        asm!("mov eax, {}",
             "mov cr3, eax",            
             "mov eax, cr0",
             "or eax, 0x80000001",
             "mov cr0, eax", in(reg) page_dir_addr, options(nostack, nomem));
    }
}
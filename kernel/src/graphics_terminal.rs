use crate::interrupts::UnintMutex;
use crate::vga_graphics::TextDrawer;
use alloc::collections::VecDeque;
use alloc::prelude::v1::*;
use alloc::vec;
use core::cmp::min;
use core::fmt;
use core::fmt::Write;
use lazy_static::lazy_static;

pub trait GlyphDraw {
    fn set_cursor(&mut self, x: usize, y: usize);
    fn draw_glyph(&mut self, ch: u8);
    fn draw_string(&mut self, s: &[u8]);
}

pub struct Terminal {
    term_w: usize,
    term_h: usize,
    in_scroll_mode: bool,
    cursor_x: usize,
    cursor_y: usize,
    screen_buffer: VecDeque<Vec<u8>>,
    drawer: Box<dyn GlyphDraw>,
}

unsafe impl Sync for Terminal {}
unsafe impl Send for Terminal {}

impl Terminal {
    pub fn new(drawer: Box<dyn GlyphDraw>, w: usize, h: usize, scroll: bool) -> Terminal {
        Terminal {
            term_w: w,
            term_h: h,
            in_scroll_mode: scroll,
            cursor_x: 0,
            cursor_y: 0,
            screen_buffer: VecDeque::with_capacity(h as usize),
            drawer: drawer,
        }
    }

    fn clear(&mut self) {
        //TODO
    }

    pub fn set_cursor(&mut self, x: usize, y: usize) {
        if self.in_scroll_mode {
            return;
        }
        self._set_cursor(x, y);
    }

    pub fn write(&mut self, s: &[u8]) {
        let till_end = (self.term_w - self.cursor_x) as usize;
        let left = min(till_end, s.len());
        if !self.in_scroll_mode {
            // just cut the remaining part and
            // prevent cursor wrapping to next line
            Self::write_text(self.drawer.as_mut(), &s[..left]);
            if left == till_end {
                self._set_cursor(self.term_w - 1, self.cursor_y);
            } else {
                self.cursor_x += left;
            }
        } else {
            self.write_text_and_save_to_buf(&s[..left]);
            if left < s.len() {
                self.line_break();
                self.write(&s[left..]);
            } else {
                self.cursor_x += s.len();
            }
        }
    }

    pub fn line_break(&mut self) {
        if self.cursor_y < self.term_h - 1 {
            self._set_cursor(0, self.cursor_y + 1);
        } else if self.in_scroll_mode {
            self.scroll_up();
        }
    }

    // fn set_forecolor(&mut self, color: u8) {
    //     //TODO
    // }

    pub fn set_autoscroll(&mut self, s: bool) {
        self.in_scroll_mode = s;
        if s {
            self._set_cursor(0, 0);
            self.screen_buffer.clear();
        }
    }

    fn scroll_up(&mut self) {
        self.screen_buffer.pop_front();
        self.screen_buffer.push_back(vec![b' '; self.term_w]);
        for (i, line) in self.screen_buffer.iter().enumerate() {
            Self::_set_cursor_drawer(self.drawer.as_mut(), 0, i);
            Self::write_text(self.drawer.as_mut(), line);
        }
        self._set_cursor(0, self.cursor_y);
    }

    fn _set_cursor_drawer(drawer: &mut dyn GlyphDraw, x: usize, y: usize) {
        drawer.set_cursor(x, y);
    }

    fn _set_cursor(&mut self, x: usize, y: usize) {
        Self::_set_cursor_drawer(self.drawer.as_mut(), x, y);
        self.cursor_x = x;
        self.cursor_y = y;
    }

    fn write_text_and_save_to_buf(&mut self, text: &[u8]) {
        while self.cursor_y >= self.screen_buffer.len() {
            self.screen_buffer.push_back(vec![b' '; self.term_w]);
        }
        let y = self.cursor_y;
        let x = self.cursor_x;
        self.screen_buffer[y][x..(x + text.len())].copy_from_slice(text);
        Self::write_text(self.drawer.as_mut(), text);
    }
    fn write_text(drawer: &mut dyn GlyphDraw, text: &[u8]) {
        drawer.draw_string(text);
    }
}

lazy_static! {
    pub static ref TERM: UnintMutex<Terminal> = UnintMutex::new({
        let td = TextDrawer::new(52);
        let term_w = td.chars_w;
        let term_h = td.chars_h;
        Terminal::new(Box::new(td), term_w, term_h, true)
    });
}

impl fmt::Write for Terminal {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        let lines: Vec<&str> = s.split('\n').collect();
        for (i, l) in lines.iter().enumerate() {
            if l.len() > 0 {
                self.write(s.as_bytes());
            }
            if i < lines.len() - 1 {
                self.line_break();
            }
        }
        Ok(())
    }
}

pub fn print_args(args: fmt::Arguments) {
    TERM.lock().write_fmt(args).unwrap();
}

#[macro_export]
macro_rules! print {
    ($($arg:tt)*) => ($crate::graphics_terminal::print_args(format_args!($($arg)*)));
}

#[macro_export]
macro_rules! println {
    () => ($crate::print!("\n"));
    ($($arg:tt)*) => ($crate::print!("{}\n", format_args!($($arg)*)));
}

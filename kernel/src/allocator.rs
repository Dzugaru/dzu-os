// As per Operating Systems: Three Easy Pieces
// https://pages.cs.wisc.edu/~remzi/OSTEP/vm-freespace.pdf
// Simple free list allocator.
// Explicit free list is always ordered by address,
// Alloc is greedy (first block greater, than requested size (+align padding))
// Free performs search to keep sorted by address invariant.
// Freed blocks are coalesced with neighbours immediately.

use crate::interrupts::UnintMutex;
use crate::println;
use alloc::alloc::{GlobalAlloc, Layout};
use alloc::prelude::v1::*;
use core::assert;
use core::cmp::max;
use core::mem::size_of;
use core::ptr::{null, null_mut};
use rand::prelude::*;
use rand_pcg::Pcg32;
use spin::Mutex;

//TODO: size must be 8 bytes
#[derive(Debug, Copy, Clone)]
#[repr(C)]
struct Block {
    len: usize,

    // in case of free block this is a pointer to the next free block,
    // in case of used block this is a pointer to the start of occupied space
    // (this is needed for alignment, we have to shift block structure and data forward,
    // and we need to keep track of the space lost)
    next: *mut Block,
}

struct BlockList {
    root: *mut Block,
}

unsafe impl Send for BlockList {}

struct Allocator {
    start_addr: usize,
    size: usize,
}

impl BlockList {
    fn init(&self) {
        let root_block = Block {
            len: ALLOC.size - size_of::<Block>(),
            next: null_mut(),
        };

        unsafe {
            self.root.write(root_block);
        }
    }

    unsafe fn alloc(&mut self, layout: Layout) -> *mut u8 {
        // Allocate greedily - first matching.
        // Align padding is considered lost (added to used space)
        let mut ptr = self.root;
        let mut prev_ptr: *mut Block = null_mut();

        loop {
            if ptr.is_null() {
                break;
            }
            let block = &*ptr;
            if block.len >= layout.size() {
                let block_start = (ptr as usize) + size_of::<Block>();
                let mut data_addr = block_start;
                // Now we need to align the data_addr according to layout
                // Suppose data_addr is 0b0010_0101 and align is 8 bytes (0b100)
                // Align mask is 0b0000_0011 and we need data addr to be 0b0010_1000
                let align_mask = layout.align() - 1;
                if (align_mask & data_addr) > 0 {
                    data_addr = (data_addr & !align_mask) + layout.align();
                }

                let align_loss = data_addr - block_start;
                let left: i32 = block.len as i32 - align_loss as i32; //NOTE: can be negative

                //println!("{} {:x} {:x}", align_loss, data_addr, block_start);
                if left >= layout.size() as i32 {
                    // We can fit even after alignment
                    let free_left = max(0, left - layout.size() as i32) as usize;
                    if free_left > size_of::<Block>() {
                        // We have free space left for new free block
                        let new_free_block = Block {
                            len: free_left - size_of::<Block>(),
                            next: block.next,
                        };
                        let new_free_block_addr = data_addr + layout.size();
                        let new_free_block_ptr = new_free_block_addr as *mut Block;
                        new_free_block_ptr.write(new_free_block);
                        if prev_ptr.is_null() {
                            self.root = new_free_block_ptr;
                        } else {
                            (*prev_ptr).next = new_free_block_ptr;
                        }
                    } else {
                        // No free space left: free block is deleted
                        if prev_ptr.is_null() {
                            self.root = block.next;
                        } else {
                            (*prev_ptr).next = block.next;
                        }
                    }
                    let final_used_len = if free_left > size_of::<Block>() {
                        align_loss + layout.size()
                    } else {
                        // If we have no space left for free block,
                        // accomodate the scraps into used to prevent space loss
                        align_loss + layout.size() + free_left
                    };

                    // Write Block
                    let used_block = Block {
                        len: final_used_len,
                        // ptr to ourself if no alignment,
                        // otherwise ptr to old free block position
                        next: ptr,
                    };

                    // Write before data, if align_loss > 0,
                    // this is not at ptr (old free block) location!
                    ((data_addr - size_of::<Block>()) as *mut Block).write(used_block);

                    return data_addr as *mut u8;
                }
            }
            // Can't fit here, search for next
            prev_ptr = ptr;
            ptr = block.next;
        }
        null_mut()
    }

    //NOTE: we don't need layout
    unsafe fn dealloc(&mut self, ptr: *mut u8, _layout: Layout) {
        let uptr = ptr.sub(size_of::<Block>()) as *mut Block;
        let used_block = &*uptr;
        // pointer to start of occupied space
        // (can be different from our position because of alignment)
        let fptr = used_block.next;
        let mut len = used_block.len;

        // Will be left None if block is not needed (coalesced with prev)
        let mut next: Option<*mut Block> = None;

        // Insert free block into list
        // Search first address, that is greater.
        // Coalescing diagram:
        // prev_ptr [8] [prev len] fptr [8] [len] ptr [8] [ptr len]
        let mut ptr = self.root;
        let mut prev_ptr: *mut Block = null_mut();
        loop {
            if ptr.is_null() {
                if prev_ptr.is_null() {
                    // Both sides missing - cant coalesce
                    self.root = fptr;
                    next = Some(null_mut());
                } else {
                    // Only left side is present, coalesce left if possible
                    if prev_ptr as usize + size_of::<Block>() + (*prev_ptr).len == fptr as usize {
                        // coalesce left
                        (*prev_ptr).len += size_of::<Block>() + len;
                    } else {
                        (*prev_ptr).next = fptr;
                        next = Some(null_mut());
                    }
                }
                break;
            }

            if ptr > fptr {
                if prev_ptr.is_null() {
                    // Only right side present, coalesce right if possible
                    if fptr as usize + size_of::<Block>() + len == ptr as usize {
                        // coalesce right
                        len += size_of::<Block>() + (*ptr).len;
                        next = Some((*ptr).next);
                    } else {
                        next = Some(self.root);
                    }
                    self.root = fptr;
                } else {
                    // Both sides present
                    let cleft =
                        prev_ptr as usize + size_of::<Block>() + (*prev_ptr).len == fptr as usize;
                    let cright = fptr as usize + size_of::<Block>() + len == ptr as usize;
                    if cleft && cright {
                        // coalesce both
                        (*prev_ptr).len += 2 * size_of::<Block>() + len + (*ptr).len;
                        (*prev_ptr).next = (*ptr).next;
                    } else if cleft {
                        // coalesce left
                        (*prev_ptr).len += size_of::<Block>() + len;
                    } else if cright {
                        // coalesce right
                        len += size_of::<Block>() + (*ptr).len;
                        next = Some((*ptr).next);
                        (*prev_ptr).next = fptr;
                    } else {
                        next = Some((*prev_ptr).next);
                        (*prev_ptr).next = fptr;
                    }
                }
                break;
            }

            prev_ptr = ptr;
            ptr = (*ptr).next;
        }

        // Write free block if needed
        if let Some(next_ptr) = next {
            fptr.write(Block {
                len: len,
                next: next_ptr,
            });
        }
    }

    unsafe fn debug_check_free_list_in_order(&self) -> usize {
        let mut ptr = self.root;
        let mut i = 0;
        loop {
            let next = (*ptr).next;
            if next.is_null() {
                break;
            }
            assert!(next > ptr);
            ptr = next;
            i += 1;
        }

        i
    }

    unsafe fn debug_walk_free_list(&self) {
        let mut ptr = self.root;
        let mut i = 0;
        let mut total_free = 0;
        loop {
            let block = &*ptr;
            total_free += block.len;

            //DEBUG
            //if i < 3 {
            println!("{} {:x?} {:x?}", i, ptr, block);
            //}
            if block.next.is_null() {
                break;
            }
            assert!(block.next > ptr, "Block idx {} wrong order", i);
            ptr = block.next;
            i += 1;
        }

        println!("Total used: {}", crate::HEAP_SIZE - total_free);
    }
}

unsafe impl GlobalAlloc for Allocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        BLOCK_LIST.lock().alloc(layout)
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {       
        BLOCK_LIST.lock().dealloc(ptr, layout);
    }
}

#[global_allocator]
static ALLOC: Allocator = Allocator {
    start_addr: crate::HEAP_ADDR,
    size: crate::HEAP_SIZE as usize,
};
static BLOCK_LIST: UnintMutex<BlockList> = UnintMutex::new(BlockList {
    root: ALLOC.start_addr as *mut Block,
});

pub fn init() {
    BLOCK_LIST.lock().init();
}

fn debug_walk_free_list() {
    unsafe { BLOCK_LIST.lock().debug_walk_free_list(); }
}

pub fn test_allocator() {
    unsafe {
        unsafe fn alloc_dummy(rng: &mut Pcg32) -> *mut u8 {
            let sz = rng.gen_range(1..129);
            let align = 1 << rng.gen_range(0..5);
            //let sz = 8;
            //let align = 1;
            let ptr = ALLOC.alloc(Layout::from_size_align(sz, align).unwrap());
            //println!("Alloc {} {} at {:x}", sz, align, ptr as usize);
            ptr
        }

        //debug_walk_free_list();

        let mut rng = Pcg32::seed_from_u64(0);
        let mut ptrs: Vec<*mut u8> = Vec::new();
        let mut max_list_len = 0;
        let mut _total_allocs = 0;
        let mut _total_deallocs = 0;

        // Alloc and free randomly
        for _ in 0..100000 {
            if ptrs.len() == 0 || rng.gen::<f32>() > 0.5 {
                ptrs.push(alloc_dummy(&mut rng));
                _total_allocs += 1;
            } else {
                let i = rng.gen_range(0..ptrs.len());
                let p = ptrs[i];
                //println!("Free {:x?}", p as usize);
                ALLOC.dealloc(p, Layout::from_size_align(1, 1).unwrap());
                ptrs.remove(i);
                _total_deallocs += 1;
            }

            //debug_walk_free_list();
            
            let list_len = BLOCK_LIST.lock().debug_check_free_list_in_order();  
            max_list_len = max(list_len, max_list_len);        
        }

        // Mostly free till empty
        loop {
            if ptrs.len() == 0 {
                break;
            } else if rng.gen::<f32>() > 0.8 {
                ptrs.push(alloc_dummy(&mut rng));
                _total_allocs += 1;
            } else {
                let i = rng.gen_range(0..ptrs.len());
                let p = ptrs[i];
                //println!("Free {:x?}", p as usize);
                ALLOC.dealloc(p, Layout::from_size_align(1, 1).unwrap());
                ptrs.remove(i);
                _total_deallocs += 1;
            }

            //debug_walk_free_list();
            
            let list_len = BLOCK_LIST.lock().debug_check_free_list_in_order();    
            max_list_len = max(list_len, max_list_len);                
        }
        drop(ptrs);

        //debug_walk_free_list();
        {
            let root = &*(BLOCK_LIST.lock().root);
            assert!(root.next.is_null());
            assert!(root.len == crate::HEAP_SIZE - size_of::<Block>());
        }

        println!("Alloc test OK, total allocs: {}, deallocs: {}, max list len: {}", 
                 _total_allocs, 
                 _total_deallocs, 
                 max_list_len);
    }
}

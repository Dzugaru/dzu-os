use crate::graphics_terminal::GlyphDraw;
use crate::println;
use crate::resources;
use crate::resources::psf;
use crate::resources::Resource;
use alloc::prelude::v1::*;
use core::ptr::write_bytes;
use lazy_static::lazy_static;
use spin::Mutex;
use volatile::Volatile;
use core::cmp::min;
use core::ptr::copy_nonoverlapping;

pub const BUFFER_HEIGHT: usize = 200;
pub const BUFFER_WIDTH: usize = 320;

#[derive(Debug)]
struct RGB(u8, u8, u8);

impl RGB {
    fn from_hex(hex_color: &str) -> RGB {
        RGB(
            u8::from_str_radix(&hex_color[1..3], 16).unwrap(),
            u8::from_str_radix(&hex_color[3..5], 16).unwrap(),
            u8::from_str_radix(&hex_color[5..7], 16).unwrap(),
        )
    }
}

struct Palette {
    colors: Vec<RGB>,
}

impl Palette {
    unsafe fn load_from_resources(res: &Resource) -> Palette {
        let mut colors: Vec<RGB> = Vec::new();
        let ptr = res.addr as *const u32;
        for i in 0..res.len / 4 {
            let u32_color = *ptr.add(i);
            let color = RGB(
                ((u32_color >> 16) & 0xff) as u8,
                ((u32_color >> 8) & 0xff) as u8,
                (u32_color & 0xff) as u8,
            );
            colors.push(color);
        }

        Palette { colors: colors }
    }

    fn find_closest_color_index(&self, hex_color: &str) -> u8 {
        let color = RGB::from_hex(hex_color);

        let mut min_err = i32::MAX;
        let mut best_i: u8 = 0;
        for (i, c) in self.colors.iter().enumerate() {
            let err = (color.0 as i32 - c.0 as i32) * (color.0 as i32 - c.0 as i32)
                + (color.1 as i32 - c.1 as i32) * (color.1 as i32 - c.1 as i32)
                + (color.2 as i32 - c.2 as i32) * (color.2 as i32 - c.2 as i32);
            if err < min_err {
                min_err = err;
                best_i = i as u8;
            }
        }

        best_i
    }
}

struct Buffer {
    pixs: &'static mut [[Volatile<u8>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

impl Buffer {
    fn set_pixel(&mut self, x: u16, y: u16, color: u8) {
        self.pixs[y as usize][x as usize].write(color);
    }

    // TODO: optimize via core ptr copy_nonoverlapping
    fn draw_font_glyph(&mut self, x: u16, y: u16, font: &psf::Font, letter: char, color: u8) {
        let g = &font.glyphs[letter as u8 as usize];
        //println!("{:?}", g);
        for (dy, row) in g.pixs.iter().enumerate() {
            for (dx, v) in row.iter().enumerate() {
                if *v {
                    //println!("{} {}", x + dx as u16, y + dy as u16);
                    self.set_pixel(
                        (x as usize * font.w + dx) as u16,
                        (y as usize * font.h + dy) as u16,
                        color,
                    );
                }
            }
        }
    }

    fn get_buf_ptr(&mut self) -> *mut u8 {
        self.pixs as *mut [[Volatile<u8>; BUFFER_WIDTH]; BUFFER_HEIGHT] as *mut u8
    }

    fn clear_pos(&mut self, font: &psf::Font, x: usize, y: usize) {
        let x0 = x * font.w;
        let y0 = y * font.h;
        let y1 = y0 + font.h;
        let ptr = self.get_buf_ptr();
        unsafe {
            for y in y0..y1 {
                write_bytes(ptr.add(x0 + y * BUFFER_WIDTH), 0, font.w);
            }
        }
    }

    fn blit_screen_from(&mut self, ptr: *const u8) {
        unsafe { copy_nonoverlapping(ptr, self.get_buf_ptr(), BUFFER_WIDTH * BUFFER_HEIGHT); }
    }
}

lazy_static! {
    static ref BUFFER: Mutex<Buffer> = Mutex::new(Buffer {
        pixs: unsafe { &mut *(0xa0000 as *mut [[Volatile<u8>; BUFFER_WIDTH]; BUFFER_HEIGHT]) }
    });
    static ref PALETTE: Palette = unsafe {
        Palette::load_from_resources(
            crate::resources::RESOURCES
                .get_by_name("default_palette.bin")
                .unwrap(),
        )
    };
}

fn draw_current_palette() {
    let bsize = 8;
    let offset = 20;
    for y in 0..16 {
        for x in 0..16 {
            let c = y * 16 + x;
            for by in 0..bsize {
                for bx in 0..bsize {
                    BUFFER.lock().set_pixel(
                        offset + x * bsize + bx,
                        offset + y * bsize + by,
                        c as u8,
                    );
                }
            }
        }
    }
}

pub struct TextDrawer {
    cursor_x: usize,
    cursor_y: usize,
    color: u8,
    font: &'static psf::Font,
    pub chars_w: usize,
    pub chars_h: usize,
}

impl TextDrawer {
    pub fn new(color: u8) -> TextDrawer {
        let font = &psf::FONT;
        TextDrawer {
            cursor_x: 0,
            cursor_y: 0,
            color: color,
            font: font,
            chars_w: BUFFER_WIDTH / font.w,
            chars_h: BUFFER_HEIGHT / font.h
        }
    }
}

impl GlyphDraw for TextDrawer {
    fn set_cursor(&mut self, x: usize, y: usize) {
        self.cursor_x = min(x, self.chars_w - 1);
        self.cursor_y = min(y, self.chars_h - 1);
    }

    fn draw_glyph(&mut self, ch: u8) {
        BUFFER
            .lock()
            .clear_pos(self.font, self.cursor_x, self.cursor_y);
        BUFFER.lock().draw_font_glyph(
            self.cursor_x as u16,
            self.cursor_y as u16,
            self.font,
            ch as char,
            self.color,
        );
    }

    fn draw_string(&mut self, s: &[u8]) {
        for ch in s {
            self.draw_glyph(*ch);
            self.set_cursor(self.cursor_x + 1, self.cursor_y);
        }
    }
}

pub fn test() {
    // for c in &PALETTE.colors {
    //     println!("{:x?}", c);
    // }
    //let color = "#ff3300";
    //let clid = PALETTE.find_closest_color_index(color);
    // println!("Closest to {} is #{}: {:x?}", color, clid, PALETTE.colors[clid as usize]);
    //draw_font_glyph(108, 100, &resources::psf::FONT, 'B', clid);
    // Draw palette
    draw_current_palette();
    

    //BUFFER.lock().clear_pos(&psf::FONT, 5, 5);
}

pub fn blit_screen_from(ptr: *const u8) {
    BUFFER.lock().blit_screen_from(ptr);
}

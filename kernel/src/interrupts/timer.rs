use crate::interrupts::without_interrupts;
use crate::asm;
use core::cmp::{min,max};
//use crate::println;

/// Sets Programmable Interval Timer (PIT) chip (Intel 8253/8254)
/// frequency divisor value (1..65535) that divides its 1.193182 MHz base freq 
pub fn set_frequency_divisor(target_freq_hz: f32) {
    let div = (1.193182e6 / target_freq_hz) as i32;
    let div = min(0xffff, max(1, div));

    //println!("{}", div);

    let lobyte: u8 = (div | 0xff) as u8;
    let hibyte: u8 = (div >> 8) as u8;
    without_interrupts(|| {
        asm::outb(0x43, 0b00110100); // Command to change rate - channel 0, lobyte/hibyte, rate generator
        asm::io_wait();
        asm::outb(0x40, lobyte);
        asm::io_wait();
        asm::outb(0x40, hibyte);
    });   
}
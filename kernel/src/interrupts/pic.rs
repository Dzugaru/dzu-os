use crate::asm::*;
use spin::Mutex;
use crate::interrupts::UnintMutex;

pub const PIC_MASTER_OFFSET: u8 = 0x20;
pub const PIC_SLAVE_OFFSET: u8 = 0x28;

pub static PICS: UnintMutex<ChainedPics> = 
        UnintMutex::new(ChainedPics { 
            master: Pic::new_master(PIC_MASTER_OFFSET),
            slave: Pic::new_master(PIC_SLAVE_OFFSET)
        });


/// An individual PIC chip.  This is not exported, because we always access
/// it through `Pics` below.
struct Pic {
    /// The base offset to which our interrupts are mapped.
    offset: u8,

    /// The processor I/O port on which we send commands.
    command: u16,

    /// The processor I/O port on which we send and receive data.
    data: u16,
}

impl Pic {
    const fn new_master(offset: u8) -> Pic {
        Pic {
            offset: offset,
            command: 0x20,
            data: 0x21
        }
    }

    const fn new_slave(offset: u8) -> Pic {
        Pic {
            offset: offset,
            command: 0xa0,
            data: 0xa1
        }
    }

    fn handles_interrupt(&self, interrupt_id: u8) -> bool {
        interrupt_id >= self.offset && interrupt_id < self.offset + 8
    }
}

pub struct ChainedPics {
    master: Pic,
    slave: Pic
}

impl ChainedPics {
    pub fn init(&self) {
        // Save our original interrupt masks, we'll restore these when we're
        // done.
        let saved_mask1 = inb(self.master.data);
        let saved_mask2 = inb(self.slave.data);

        // Tell each PIC that we're going to send it a three-byte
        // initialization sequence on its data port.
        outb(self.master.command, 0x11);
        io_wait();
        outb(self.slave.command, 0x11);
        io_wait();

        // Byte 1: Set up our base offsets.
        outb(self.master.data, self.master.offset);        
        io_wait();
        outb(self.slave.data, self.slave.offset);
        io_wait();

        // Byte 2: Configure chaining between PIC1 and PIC2.
        // tell Master PIC that there is a slave PIC at IRQ2 (0000 0100)
        outb(self.master.data, 0b0000_0100);        
        io_wait();
        // tell Slave PIC its cascade identity (0000 0010)
        outb(self.slave.data, 0b0000_0010);
        io_wait();

        // Byte 3: Set our mode (8086)
        outb(self.master.data, 1);        
        io_wait();
        outb(self.slave.data, 1);
        io_wait();

        // Restore our saved masks.
        outb(self.master.data, saved_mask1);
        outb(self.slave.data, saved_mask2);       
    }

    pub fn notify_end_of_interrupt(&self, interrupt_id: u8) {
        if self.slave.handles_interrupt(interrupt_id) {
            outb(self.slave.command, 0x20);
        }
        outb(self.master.command, 0x20);
    }
}
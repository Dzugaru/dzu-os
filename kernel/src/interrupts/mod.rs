use crate::{print, println};
use core::cmp::{max, min};
use lazy_static::lazy_static;
use spin::{Mutex, MutexGuard};

mod idt;
mod pic;
mod privilege_level;
pub mod timer;

use super::asm;
use super::keyboard;
use crate::vga_graphics;

use pic::PICS;

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum ExceptionIndex {
    DivisionByZero = 0,
    Breakpoint = 3,
    InvalidOpCode = 6,
    DoubleFault = 8,
    GeneralProtectionFault = 13,
    PageFault = 14,
}

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum InterruptIndex {
    Timer = pic::PIC_MASTER_OFFSET,
    Keyboard = pic::PIC_MASTER_OFFSET + 1,
    CascadeNeverUsed = pic::PIC_MASTER_OFFSET + 2,
    COM2 = pic::PIC_MASTER_OFFSET + 3,
    COM1 = pic::PIC_MASTER_OFFSET + 4,
    LPT2 = pic::PIC_MASTER_OFFSET + 5,
    Floppy = pic::PIC_MASTER_OFFSET + 6,
    LPT1Spurious = pic::PIC_MASTER_OFFSET + 7,
    CMOSClock = pic::PIC_SLAVE_OFFSET,
    Free9 = pic::PIC_SLAVE_OFFSET + 1,
    Free10 = pic::PIC_SLAVE_OFFSET + 2,
    Free11 = pic::PIC_SLAVE_OFFSET + 3,
    PS2Mouse = pic::PIC_SLAVE_OFFSET + 4,
    FPU = pic::PIC_SLAVE_OFFSET + 5,
    PrimaryATA = pic::PIC_SLAVE_OFFSET + 6,
    SecondaryATA = pic::PIC_SLAVE_OFFSET + 7,
}

macro_rules! handler {
    ($name:ident) => {{
        #[naked]
        extern "C" fn wrapper() -> ! {
            unsafe {
                //TODO: align stack to 16byte? or not needed in x86?
                asm!("push eax", //we need to save caller's saved registers
                     "push ecx", //because we know callee (our handler) will not save them,
                     "push edx", //we rely on callee to save other ones
                     "mov ecx, esp", //save esp (address of interrupt frame) and pass it to handler
                     "add ecx, 12", //correct for pushes above
                     "push ecx", //pass as first argument
                     "call {}",
                     "add esp,4", //caller is responsible for cleaning func args
                     "pop edx",
                     "pop ecx",
                     "pop eax",
                     "iretd", //NOTE: iret compiles as iretw (16bit variant) for some reason
                     sym $name, options(noreturn));
            }
        }
        wrapper
    }};
}

// extern "C" fn H2(_stack_frame: *const InterruptStackFrame) { println!("IRQ 2"); }
// extern "C" fn H3(_stack_frame: *const InterruptStackFrame) { println!("IRQ 3"); }
// extern "C" fn H4(_stack_frame: *const InterruptStackFrame) { println!("IRQ 4"); }
// extern "C" fn H5(_stack_frame: *const InterruptStackFrame) { println!("IRQ 5"); }
// extern "C" fn H6(_stack_frame: *const InterruptStackFrame) { println!("IRQ 6"); }
// extern "C" fn H7(_stack_frame: *const InterruptStackFrame) { println!("IRQ 7"); }
// extern "C" fn H8(_stack_frame: *const InterruptStackFrame) { println!("IRQ 8"); }
// extern "C" fn H9(_stack_frame: *const InterruptStackFrame) { println!("IRQ 9"); }
// extern "C" fn H10(_stack_frame: *const InterruptStackFrame) { println!("IRQ 10"); }
// extern "C" fn H11(_stack_frame: *const InterruptStackFrame) { println!("IRQ 11"); }
// extern "C" fn H12(_stack_frame: *const InterruptStackFrame) { println!("IRQ 12"); }
// extern "C" fn H13(_stack_frame: *const InterruptStackFrame) { println!("IRQ 13"); }
// extern "C" fn H14(_stack_frame: *const InterruptStackFrame) { println!("IRQ 14"); }
// extern "C" fn H15(_stack_frame: *const InterruptStackFrame) { println!("IRQ 15"); }

extern "C" fn H2(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H3(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H4(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H5(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H6(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H7(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H8(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H9(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H10(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H11(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H12(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H13(_stack_frame: *const InterruptStackFrame) {  }
extern "C" fn H14(_stack_frame: *const InterruptStackFrame) { }
extern "C" fn H15(_stack_frame: *const InterruptStackFrame) {  }


//TODO check type of wrapped function $name is correct somehow!
// Adds error code
macro_rules! handler_err {
    ($name: ident) => {{
        #[naked]
        extern "C" fn wrapper() -> ! {
            unsafe {
                //TODO: align stack to 16byte? or not needed in x86?
                asm!("mov ecx, esp", //save esp and pass it to handler
                     "add ecx, 4", //error code was already pushed (as first argument)
                     "push ecx", //so we just modify ecx by 4 bytes to point to second arg
                     "call {}", sym $name, options(noreturn));
            }
        }
        wrapper
    }}
}

lazy_static! {
    static ref IDT: idt::Idt = {
        let mut idt = idt::Idt::new();
        idt.set_handler(
            ExceptionIndex::DivisionByZero as u8,
            1,
            handler!(divide_by_zero_handler),
        );
        idt.set_handler(
            ExceptionIndex::Breakpoint as u8,
            1,
            handler!(breakpoint_handler),
        );
        idt.set_handler(
            ExceptionIndex::InvalidOpCode as u8,
            1,
            handler!(invalid_opcode_handler),
        );
        idt.set_handler(
            ExceptionIndex::DoubleFault as u8,
            1,
            handler_err!(double_fault_handler),
        );
        idt.set_handler(
            ExceptionIndex::PageFault as u8,
            1,
            handler_err!(page_fault_handler),
        );
        idt.set_handler(
            ExceptionIndex::GeneralProtectionFault as u8,
            1,
            handler_err!(gpf_handler),
        );
        idt.set_handler(InterruptIndex::Timer as u8, 1, handler!(timer_handler));
        idt.set_handler(
            InterruptIndex::Keyboard as u8,
            1,
            handler!(keyboard_handler),
        );

        idt.set_handler(InterruptIndex::CascadeNeverUsed as u8, 1, handler!(H2));
        idt.set_handler(InterruptIndex::COM2 as u8, 1, handler!(H3));
        idt.set_handler(InterruptIndex::COM1 as u8, 1, handler!(H4));
        idt.set_handler(InterruptIndex::LPT2 as u8, 1, handler!(H5));
        idt.set_handler(InterruptIndex::Floppy as u8, 1, handler!(H6));
        idt.set_handler(InterruptIndex::LPT1Spurious as u8, 1, handler!(H7));
        idt.set_handler(InterruptIndex::CMOSClock as u8, 1, handler!(H8));
        idt.set_handler(InterruptIndex::Free9 as u8, 1, handler!(H9));
        idt.set_handler(InterruptIndex::Free10 as u8, 1, handler!(H10));
        idt.set_handler(InterruptIndex::Free11 as u8, 1, handler!(H11));
        idt.set_handler(InterruptIndex::PS2Mouse as u8, 1, handler!(H12));
        idt.set_handler(InterruptIndex::FPU as u8, 1, handler!(H13));
        idt.set_handler(InterruptIndex::PrimaryATA as u8, 1, handler!(H14));
        idt.set_handler(InterruptIndex::SecondaryATA as u8, 1, handler!(H15));
    
        idt
    };
}

static mut COUNTER: u32 = 0;

pub fn init() {
    IDT.load();    
    PICS.lock().init();
}

#[inline(never)]
extern "C" fn breakpoint_handler(stack_frame: *const InterruptStackFrame) {
    println!("\nBREAKPOINT (INT 3)\n{:#x?}", unsafe { &*stack_frame });
}

#[inline(never)]
extern "C" fn divide_by_zero_handler(stack_frame: *const InterruptStackFrame) -> ! {
    println!("\nEXCEPTION: DIVIDE BY ZERO\n{:#x?}", unsafe {
        &*stack_frame
    });
    crate::halt_loop();
}

#[inline(never)]
extern "C" fn invalid_opcode_handler(stack_frame: *const InterruptStackFrame) -> ! {
    println!("\nEXCEPTION: INVALID OPCODE\n{:#x?}", unsafe {
        &*stack_frame
    });
    crate::halt_loop();
}

#[inline(never)]
extern "C" fn page_fault_handler(stack_frame: *const InterruptStackFrame, error_code: u32) -> ! {
    println!(
        "\nEXCEPTION: PAGE FAULT with error code {:?}\n{:#x?}",
        error_code,
        unsafe { &*stack_frame }
    );
    crate::halt_loop();
}

#[inline(never)]
extern "C" fn gpf_handler(stack_frame: *const InterruptStackFrame, error_code: u32) -> ! {
    println!(
        "\nEXCEPTION: GENERAL PROTECTION FAULT with error code {:?}\n{:#x?}",
        error_code,
        unsafe { &*stack_frame }
    );
    crate::halt_loop();
}

#[inline(never)]
extern "C" fn double_fault_handler(stack_frame: *const InterruptStackFrame, error_code: u32) -> ! {
    println!(
        "\nEXCEPTION: DOUBLE FAULT with error code {:?}\n{:#x?}",
        error_code,
        unsafe { &*stack_frame }
    );
    crate::halt_loop();
}

#[inline(never)]
extern "C" fn timer_handler(_stack_frame: *const InterruptStackFrame) {  
    {
        let mut time_counter = crate::TIME_MS.lock();
        *time_counter = *time_counter + 1;
    }  
    
    PICS.lock().notify_end_of_interrupt(InterruptIndex::Timer as u8);
}

#[inline(never)]
extern "C" fn keyboard_handler(_stack_frame: *const InterruptStackFrame) {
    let scancode = asm::inb(0x60);
    let result = keyboard::key_event_from_scancode(scancode);
    if let Ok(k) = result {

        //TODO: deadlocks in tetris if uncommented?
        //print!("{:?} {:?}", k.code, k.tp);
        //println!("{}", *crate::TIME_MS.lock());
        crate::KEYBOARD_INPUT.lock().push_back(k);
    }

    PICS.lock().notify_end_of_interrupt(InterruptIndex::Keyboard as u8);
}

#[derive(Debug)]
#[repr(C)]
struct InterruptStackFrame {
    instruction_pointer: u32,
    code_segment: u32,
    cpu_flags: u32,
}

#[inline]
pub fn without_interrupts<F, R>(f: F) -> R
where
    F: FnOnce() -> R,
{
    asm::cli();
    let ret = f();
    asm::sti();
    ret
}

// Mutex, that disables interrupts before lock.
// You generally want to do that because if interrupt handler
// tries to enter critical section and interrupt happened inside
// a critical section the system will deadlock.
pub struct UnintMutex<T> {
    m: Mutex<T>,
}

pub struct UnintMutexGuard<'a, T> {
    enable_back: bool,
    mguard: Option<MutexGuard<'a, T>>,
}

impl<T> UnintMutex<T> {
    pub fn lock(&self) -> UnintMutexGuard<T> {
        let int_enabled = asm::are_interrupts_enabled(); 
        if int_enabled {
            asm::cli();
        }
        UnintMutexGuard {
            enable_back: int_enabled,
            mguard: Some(self.m.lock())
        }
    }

    pub const fn new(x: T) -> UnintMutex<T> {
        UnintMutex { m: Mutex::new(x) }
    }
}

impl<T> Drop for UnintMutexGuard<'_, T> {
    fn drop(&mut self) {
        self.mguard = None; // dropping the guard will release the lock
        if self.enable_back {
            // we won't enable interrupts that were not enabled before
            asm::sti();
        }
    }
}

use core::ops::{Deref, DerefMut};

impl<T> Deref for UnintMutexGuard<'_, T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        self.mguard.as_deref().unwrap()
    }
}

impl<T> DerefMut for UnintMutexGuard<'_, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.mguard.as_deref_mut().unwrap()
    }
}


#![no_std]
#![no_main]
#![feature(asm)]
#![feature(naked_functions)]
#![allow(dead_code)]
#![allow(unused_imports)]
#![feature(alloc_error_handler)]
#![feature(alloc_prelude)]

extern crate alloc;

const STACK_ADDR: usize = 0x0fc0_0000; // stack base is at 0x0fffffff
const HEAP_ADDR: usize = 0x0100_0000;
const HEAP_SIZE: usize = 0x0040_0000;

const KERNEL_BASE: usize = 0x10000;
const KERNEL_SIZE: usize = 832 * 512;

mod allocator;
mod asm;
mod games;
mod graphics_terminal;
mod interrupts;
mod keyboard;
mod paging;
mod resources;
mod vga_graphics;
mod vga_text;

use alloc::collections::VecDeque;
use alloc::prelude::v1::*;
use alloc::vec;
use core::panic::PanicInfo;
use interrupts::UnintMutex;
use lazy_static::lazy_static;

#[no_mangle]
pub extern "C" fn _start() -> ! {
    //println!("{}", asm::are_interrupts_enabled());
    interrupts::init();
    interrupts::timer::set_frequency_divisor(1000.0);
    asm::sti();

    //println!("{}", asm::are_interrupts_enabled());
    paging::init();
    allocator::init();
    allocator::test_allocator();

    //println!("TEST3");

    let mut game_engine = games::GameEngine::new(60.);
    let mut tetris = games::tetris::Tetris::new();

    loop {
        game_engine.step(&mut tetris);
    }

    //unsafe { asm!("int 3"); }
    //invalid opcode
    //unsafe { asm!("ud2") };

    // //divide_by_zero();

    //divide by zero
    // unsafe { asm!("mov edx, 0",
    //               "mov eax, 0x8003",
    //               "mov ecx, 0x0",
    //               "div ecx"); }

    //panic!("PANIC!!!!");

    crate::halt_loop();
}

#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    crate::halt_loop();
}

#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}

pub fn halt_loop() -> ! {
    loop {
        asm::hlt();
    }
}

static TIME_MS: UnintMutex<u64> = UnintMutex::new(0u64);

lazy_static! {
    static ref KEYBOARD_INPUT: UnintMutex<VecDeque<keyboard::KeyEvent>> =
        UnintMutex::new(VecDeque::new());
}

pub fn wait(ms: u64) {
    let target_time = *TIME_MS.lock() + ms;
    loop {
        asm::hlt();
        let curr_time = *TIME_MS.lock();
        if curr_time >= target_time {
            break;
        }
    }
}

use crate::println;
use alloc::prelude::v1::*;
use core::mem::size_of;
use core::str::from_utf8;
use lazy_static::lazy_static;

pub mod psf;

#[derive(Debug, Copy, Clone)]
#[repr(C, packed)]
struct TableEntry {
    name: [u8; 28],
    addr: [u8; 4],
    len: [u8; 4],
}

#[derive(Debug)]
pub struct Resource {
    pub name: &'static str,
    pub addr: *const u8,
    pub len: usize,
}

pub struct ResourceCollection {
    pub resources: Vec<Resource>,
}

unsafe impl Sync for Resource {}
unsafe impl Send for Resource {}

impl ResourceCollection {
    pub fn get_by_name(&self, name: &str) -> Option<&Resource> {
        self.resources.iter().find(|r| { r.name == name })
    }

    fn new() -> ResourceCollection {
        let mut rs: Vec<Resource> = Vec::new();

        unsafe {
            // jump to start using total len at the end
            let end_addr = crate::KERNEL_BASE + crate::KERNEL_SIZE - size_of::<usize>();
            let res_len = *(end_addr as *const usize);
            let start_addr = end_addr - res_len;
            // jump to table
            let table_addr = start_addr + *(start_addr as *const usize);
            let table_len = *(table_addr as *const usize);
            let table_ptr = (table_addr + size_of::<usize>()) as *const TableEntry;
            for i in 0..table_len {
                let e = &*table_ptr.add(i);
                let name_len = e.name.iter().position(|x| *x == 0).unwrap();
                let name = from_utf8(&e.name[0..name_len]).unwrap();
                let addr = *((&e.addr as *const u8) as *const usize);
                let len = *((&e.len as *const u8) as *const usize);

                let r = Resource {
                    name: name,
                    addr: (start_addr + addr) as *const u8,
                    len: len,
                };
                rs.push(r);
            }
        }

        ResourceCollection { resources: rs }
    }
}

lazy_static! {
    pub static ref RESOURCES: ResourceCollection = ResourceCollection::new();
}

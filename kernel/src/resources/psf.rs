use crate::println;
use crate::resources;
use crate::resources::Resource;
use alloc::prelude::v1::*;
use core::mem::size_of;
use core::slice::from_raw_parts;
use lazy_static::lazy_static;

#[derive(Debug)]
struct PSF1Header {
    magic: u16,
    mode: u8,
    height: u8,
}

#[derive(Debug)]
pub struct Glyph {
    pub pixs: Vec<Vec<bool>>,
}

#[derive(Debug)]
pub struct Font {
    pub glyphs: Vec<Glyph>,
    pub w: usize,
    pub h: usize,
}

impl Font {
    unsafe fn from_resource(res: &Resource, n_glyphs: u32) -> Font {
        let mut glyphs: Vec<Glyph> = Vec::new();

        let header = &*(res.addr as *const PSF1Header);
        let mut offset = res.addr.add(size_of::<PSF1Header>());
        for _ in 0..n_glyphs {
            let mut glyph = Glyph { pixs: Vec::new() };

            let raw_glyph = from_raw_parts(offset, header.height as usize);
            for rb in raw_glyph {
                let mut row: Vec<bool> = Vec::new();
                let mut mask = 0b1000_0000;
                for _ in 0..8 {
                    row.push(mask & rb > 0);
                    mask >>= 1;
                }
                glyph.pixs.push(row);
            }
            offset = offset.add(header.height as usize);
            glyphs.push(glyph);
        }

        Font {
            glyphs: glyphs,
            w: 8,
            h: header.height as usize,
        }
    }
}

lazy_static! {
    pub static ref FONT: Font = {
        if let Some(res) = resources::RESOURCES.get_by_name("font.psf") {
            unsafe { Font::from_resource(res, 128) }
        } else {
            panic!("No font.psf in resources!")
        }
    };
}

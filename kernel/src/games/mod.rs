use crate::vga_graphics::BUFFER_HEIGHT;
use crate::vga_graphics::BUFFER_WIDTH;
use crate::TIME_MS;
use crate::KEYBOARD_INPUT;
use core::ptr::write_bytes;
use crate::keyboard::KeyEvent;
use alloc::vec::Vec;

pub mod tetris;

pub struct ScreenBuf {
    buf: [[u8; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

pub struct GameEngine {
    screen_buf: ScreenBuf,
    frame_dur: u64,
    should_stop: bool,
    next_frame_time: u64,
}

pub trait Game {
    fn update(&mut self, key_events: Vec<KeyEvent>);
    fn draw(&mut self, buf: &mut ScreenBuf);
}

impl GameEngine {
    pub fn step<T: Game>(&mut self, game: &mut T) { 
        let mut key_events: Vec<KeyEvent> = Vec::new();
        {               
            let mut input = KEYBOARD_INPUT.lock();    
            loop {
                if let Some(e) = input.pop_front() {
                    key_events.push(e);
                } else {
                    break;
                }                
            }               
        }
        game.update(key_events);
        self.clear_buffer();
        game.draw(&mut self.screen_buf);
        let time = *TIME_MS.lock();
        if time < self.next_frame_time {
            crate::wait(self.next_frame_time - time);
        }
        self.blit_buffer_to_screen();
        self.next_frame_time += self.frame_dur;
    }

    pub fn new(fps: f32) -> GameEngine {
        let frame_dur = (1000. / fps) as u64;
        GameEngine {
            screen_buf: ScreenBuf {
                buf: [[0u8; BUFFER_WIDTH]; BUFFER_HEIGHT],
            },
            frame_dur: frame_dur,
            should_stop: false,
            next_frame_time: *TIME_MS.lock() + frame_dur,
        }
    }

    fn get_pointer_to_buf_mut(&mut self) -> *mut u8 {
        &mut self.screen_buf.buf as *mut [[u8; BUFFER_WIDTH]; BUFFER_HEIGHT] as *mut u8
    }

    fn get_pointer_to_buf(&self) -> *const u8 {
        &self.screen_buf.buf as *const [[u8; BUFFER_WIDTH]; BUFFER_HEIGHT] as *const u8
    }

    fn blit_buffer_to_screen(&self) {
        crate::vga_graphics::blit_screen_from(
            self.get_pointer_to_buf()
        );
    }

    fn clear_buffer(&mut self) {
        unsafe { write_bytes(self.get_pointer_to_buf_mut(), 0, BUFFER_HEIGHT * BUFFER_WIDTH); }
    }
}

use lazy_static::lazy_static;
use super::{Fig, FigVar};
use alloc::prelude::v1::*;
use alloc::vec;

macro_rules! figure {
        ($([$($line:literal),+]);+; center $cent:expr) => {{
            let mut vars: Vec<FigVar> = Vec::new();
    
            $(
                vars.push(FigVar::from_macro_lines(vec![$($line),+], $cent));
            )+
    
            Fig {
                vars: vars
            }
        }};
    }

lazy_static! {
    pub(super) static ref FIGURES: Vec<Fig> = {
        vec![
            figure!(["  0",
                     "000"];
                    
                    [" 0 ",
                     " 0 ",
                     " 00"];
                    
                    ["   ",
                     "000",
                     "0  "];
                    
                    ["00",
                     " 0",
                     " 0"];

                    center (1,1)),
            
            figure!(["   ",
                     "000",
                     "  0"];
                   
                    [" 0",
                     " 0",
                     "00"];
                   
                    ["0  ",
                     "000"];
                   
                    [" 00",
                     " 0 ",
                     " 0 "];

                    center (1,1)),

            figure!(["   ",
                     "000",
                     " 0 "];
                 
                    [" 0",
                     "00",
                     " 0"];
                 
                    [" 0 ",
                     "000"];
                 
                    [" 0 ",
                     " 00",
                     " 0 "];

                    center (1,1)),

            figure!([" 00",
                     "00 "];
                    
                    ["0 ",
                     "00",
                     " 0"];
                     
                    center (1,1)),

            figure!(["00 ",
                     " 00"];
                   
                    [" 0",
                     "00",
                     "0 "];
                    
                    center (1,1)),

            figure!(["00",
                     "00"];
                   
                    center (0,0)),
            
            figure!(["    ",
                     "0000"];
                  
                    [" 0",
                     " 0",
                     " 0",
                     " 0"];
                   
                    center (1,1)),
        ]
    };    
}
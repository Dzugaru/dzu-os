use crate::keyboard::{KeyCode, KeyEvent, KeyEventType};
use crate::vga_graphics::{BUFFER_HEIGHT, BUFFER_WIDTH};
use alloc::prelude::v1::*;
use core::cmp::{max, min};
use rand::prelude::*;
use rand_pcg::Pcg32;

const WELL_WIDTH: usize = 10;
const WELL_HEIGHT: usize = 23;
const WELL_WALLS: (usize, usize, usize, usize) = (120, 10, 200, 194);
const SPEED_TICKS: usize = 10;
const COLORS: [u8;6] = [1,2,3,4,5,6];

pub struct Tetris {
    falling_fig: Option<FallingFig>,
    well: Well,
    fall_ticks: usize,
    rng: Pcg32,
}

struct DrawSettings<'a> {
    screen_buf: &'a mut super::ScreenBuf,
    offset_pix: (i32, i32),
    cell_sz: usize,
    cell_margin: usize,
    color: u8,
}

struct FallingFig {
    tp: usize,
    var: usize,
    pos_x: i32,
    pos_y: i32,
    color: u8,
}

struct Well {
    blocks: [[Option<WellBlock>; WELL_WIDTH]; WELL_HEIGHT],
}

#[derive(Clone, Copy)]
struct WellBlock {
    color: u8,
}

struct FigVar {
    parts: Vec<(i32, i32)>,
}

struct Fig {
    vars: Vec<FigVar>,
}

impl FigVar {
    fn from_macro_lines(lines: Vec<&str>, center: (i32, i32)) -> FigVar {
        let mut coords: Vec<(i32, i32)> = Vec::new();

        for (i, l) in lines.into_iter().enumerate() {
            for (j, s) in l.chars().enumerate() {
                if s != ' ' {
                    coords.push((j as i32 - center.0, i as i32 - center.1));
                }
            }
        }

        FigVar { parts: coords }
    }

    fn draw(&self, draw: &mut DrawSettings, pos: (i32, i32)) {
        for c in &self.parts {
            Tetris::draw_cell(draw, (pos.0 + c.0, pos.1 + c.1));
        }
    }
}

impl FallingFig {
    fn get_fig_var(&self) -> &FigVar {
        &FIGURES[self.tp].vars[self.var]
    }

    fn get_fig_next_var(&self) -> &FigVar {
        let vars = &FIGURES[self.tp].vars;
        &vars[(self.var + 1) % vars.len()]
    }

    fn is_colliding_at_pos(&self, tetris: &Tetris, pos: (i32, i32)) -> bool {
        self.get_fig_var()
            .parts
            .iter()
            .any(|p| tetris.is_cell_colliding((pos.0 + p.0, pos.1 + p.1)))
    }

    fn is_colliding_at_pos_next_var(&self, tetris: &Tetris, pos: (i32, i32)) -> bool {
        self.get_fig_next_var()
            .parts
            .iter()
            .any(|p| tetris.is_cell_colliding((pos.0 + p.0, pos.1 + p.1)))
    }

    fn rotate(&mut self) {
        let vars = &FIGURES[self.tp].vars;
        self.var = (self.var + 1) % vars.len();
    }

    fn draw(&self, draw: &mut DrawSettings) {
        self.get_fig_var().draw(draw, (self.pos_x, self.pos_y));
    }
}

impl Well {
    fn new() -> Well {
        Well {
            blocks: [[None; WELL_WIDTH]; WELL_HEIGHT],
        }
    }

    fn fixate_fig(&mut self, fig: &FallingFig) {
        for p in &fig.get_fig_var().parts {
            self.blocks[(fig.pos_y + p.1) as usize][(fig.pos_x + p.0) as usize] =
                Some(WellBlock { color: fig.color });
        }
    }

    fn score_rows(&mut self) {
        for i in 0..WELL_HEIGHT {
            if self.blocks[i].iter().all(|b| b.is_some()) {
                for j in (0..i).rev() {
                    self.blocks[j+1] = self.blocks[j];
                }
            }
        }
    }
}

mod figures;
use figures::FIGURES;

impl super::Game for Tetris {
    fn update(&mut self, key_events: Vec<KeyEvent>) {
        if self.falling_fig.is_some() {
            for e in key_events {
                if e.tp == KeyEventType::Release {
                    continue;
                }
                if e.code == KeyCode::Numpad4 {
                    self.move_fig_left();
                }
                if e.code == KeyCode::Numpad6 {
                    self.move_fig_right();
                }
                if e.code == KeyCode::Numpad2 {
                    self.rotate_fig();
                }
                if e.code == KeyCode::Spacebar {
                    self.force_fig_down();
                }
            }

            self.fall_ticks -= 1;
            let fixated;
            if self.fall_ticks == 0 {
                fixated = self.fall();
                self.fall_ticks = SPEED_TICKS; //TODO: variable speed
            } else {
                fixated = false;
            }

            if fixated {
                self.falling_fig = None;
                self.well.score_rows();
            }
        } else {
            let new_type = (self.rng.next_u32() as usize) % FIGURES.len();
            let new_rot = (self.rng.next_u32() as usize) % FIGURES[new_type].vars.len();
            let new_color = COLORS[(self.rng.next_u32() as usize) % COLORS.len()];

            self.falling_fig = Some(FallingFig {
                tp: new_type,
                var: new_rot,
                pos_x: 5,
                pos_y: 3,
                color: new_color,
            });
        }
    }

    fn draw(&mut self, buf: &mut super::ScreenBuf) {
        Self::draw_well_walls(buf);

        if let Some(fig) = &self.falling_fig {
            fig.draw(&mut DrawSettings {
                screen_buf: buf,
                offset_pix: (WELL_WALLS.0 as i32, WELL_WALLS.1 as i32),
                cell_sz: 8,
                cell_margin: 1,
                color: fig.color,
            });
        }

        for i in 0..WELL_HEIGHT {
            for j in 0..WELL_WIDTH {
                if let Some(b) = self.well.blocks[i][j] {
                    Self::draw_cell(&mut DrawSettings {
                        screen_buf: buf,
                        offset_pix: (WELL_WALLS.0 as i32, WELL_WALLS.1 as i32),
                        cell_sz: 8,
                        cell_margin: 1,
                        color: b.color,
                    }, (j as i32,i as i32));
                }
            }
        }
    }
}

impl Tetris {
    pub fn new() -> Tetris {
        Tetris {
            falling_fig: None,
            well: Well::new(),
            fall_ticks: SPEED_TICKS,
            rng: Pcg32::seed_from_u64(0),
        }
    }

    fn is_cell_colliding(&self, pos: (i32, i32)) -> bool {
        pos.0 < 0
            || pos.0 >= WELL_WIDTH as i32
            || pos.1 < 0
            || pos.1 >= WELL_HEIGHT as i32
            || self.well.blocks[pos.1 as usize][pos.0 as usize].is_some()
    }
    
    fn fig_as_ref(&self) -> &FallingFig {
        self.falling_fig.as_ref().unwrap()
    }

    fn fig_as_mut(&mut self) -> &mut FallingFig {
        self.falling_fig.as_mut().unwrap()
    }

    fn move_fig_left(&mut self) {
        //TODO: convenience methods? or maybe without Option (value and bool)?
        //or dont pass whole Tetris in is_colliding_at_pos?
        let fig = self.fig_as_ref();
        if !fig.is_colliding_at_pos(self, (fig.pos_x - 1, fig.pos_y)) {
            self.fig_as_mut().pos_x -= 1;
        } 
    }

    fn move_fig_right(&mut self) {
        let fig = self.fig_as_ref();
        if !fig.is_colliding_at_pos(self, (fig.pos_x + 1, fig.pos_y)) {
            self.fig_as_mut().pos_x += 1;
        }
    }

    fn rotate_fig(&mut self) {
        let fig = self.fig_as_ref();
        if !fig.is_colliding_at_pos_next_var(self, (fig.pos_x, fig.pos_y)) {
            self.fig_as_mut().rotate();
        }
    }

    fn force_fig_down(&mut self) {
        let fig = self.fig_as_ref();
        let mut vertical_shift = 0;
        loop {            
            if fig.is_colliding_at_pos(self, (fig.pos_x, fig.pos_y + vertical_shift + 1)) {
                break;
            }
            vertical_shift += 1;
        }

        if vertical_shift > 0 {
            self.fig_as_mut().pos_y += vertical_shift;
            self.fall_ticks = SPEED_TICKS;
        }
    }

    fn can_fig_move_down(&mut self) -> bool {
        let fig = self.fig_as_ref();
        !fig.is_colliding_at_pos(self, (fig.pos_x, fig.pos_y + 1))
    }

    fn fall(&mut self) -> bool {
        if self.can_fig_move_down() {
            self.fig_as_mut().pos_y += 1;
            false
        } else {           
            //NOTE: cannot use self.fig_as_ref() here cause this borrows self
            self.well.fixate_fig(self.falling_fig.as_ref().unwrap());
            true
        }
    }

    fn draw_cell(draw: &mut DrawSettings, pos: (i32, i32)) {
        let cell_range = draw.cell_margin..draw.cell_sz - draw.cell_margin;
        for i in cell_range.clone() {
            for j in cell_range.clone() {
                let x = max(
                    0,
                    min(
                        BUFFER_WIDTH as i32 - 1,
                        draw.offset_pix.0 + pos.0 * (draw.cell_sz as i32) + (j as i32),
                    ),
                );
                let y = max(
                    0,
                    min(
                        BUFFER_HEIGHT as i32 - 1,
                        draw.offset_pix.1 + pos.1 * (draw.cell_sz as i32) + (i as i32),
                    ),
                );
                draw.screen_buf.buf[y as usize][x as usize] = draw.color;
            }
        }
    }

    fn draw_well_walls(buf: &mut super::ScreenBuf) {
        Self::draw_line(
            buf,
            (WELL_WALLS.0 - 1, WELL_WALLS.1 - 1),
            (WELL_WALLS.2, WELL_WALLS.1 - 1),
            1,
            15,
        );
        Self::draw_line(
            buf,
            (WELL_WALLS.0 - 1, WELL_WALLS.3),
            (WELL_WALLS.2, WELL_WALLS.3),
            1,
            15,
        );
        Self::draw_line(
            buf,
            (WELL_WALLS.0 - 1, WELL_WALLS.1 - 1),
            (WELL_WALLS.0 - 1, WELL_WALLS.3),
            1,
            15,
        );
        Self::draw_line(
            buf,
            (WELL_WALLS.2, WELL_WALLS.1 - 1),
            (WELL_WALLS.2, WELL_WALLS.3),
            1,
            15,
        );
    }

    //TODO: bounds check and move to games lib
    fn draw_line(
        buf: &mut super::ScreenBuf,
        start: (usize, usize),
        end: (usize, usize),
        thick: usize,
        color: u8,
    ) {
        let (t0, t1) = if thick % 2 == 0 {
            (thick / 2, thick / 2)
        } else {
            (thick / 2, thick / 2 + 1)
        };
        if start.0 == end.0 {
            //Vertical line
            for y in start.1..end.1 {
                for x in start.0 - t0..start.0 + t1 {
                    buf.buf[y][x] = color;
                }
            }
        } else if start.1 == end.1 {
            //Horiz line
            for y in start.1 - t0..start.1 + t1 {
                for x in start.0..end.0 {
                    buf.buf[y][x] = color;
                }
            }
        } else {
            panic!("Can't draw non-straight lines! {:?} {:?}", start, end);
        }
    }
}

use lazy_static::lazy_static;
use spin::Mutex;
use crate::println;
use crate::asm::enable_paging;
use crate::interrupts::{without_interrupts, UnintMutex};

#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
struct PageDirectoryEntry(usize);

#[derive(Clone, Copy, Debug)]
#[repr(transparent)]
struct PageTableEntry(usize);

#[repr(align(4096))]
struct PageDirectory {
    entries: [PageDirectoryEntry; 1024],
}

#[repr(align(4096))]
struct PageTable {
    entries: [PageTableEntry; 1024],
}

#[repr(align(4096))]
struct FixedPageTables {
    first_mb: PageTable,
    stack: PageTable,
    heap: PageTable,
}

impl PageDirectoryEntry {
    fn new(pt_addr: u32) -> Self {
        let flags = 0b0000_0000_0010; //not present and read/write
        let pt_addr = pt_addr & 0xFFFFF000; //align addr to 4kb
        PageDirectoryEntry((flags | pt_addr) as usize)
    }

    fn set_table_addr(&mut self, table_addr: usize) {
        let table_addr = table_addr & 0xFFFFF000; //align addr to 4kb
        self.0 &= 0x00000FFF;
        self.0 |= table_addr;
    }

    fn set_present(&mut self, is_present: bool) {
        if is_present {
            self.0 |= 0x1;
        } else {
            self.0 &= 0xffff_fffe;
        }
    }
}

impl PageTableEntry {
    fn new(page_addr: usize) -> Self {
        let flags = 0b0000_0000_0010; //not present and read/write
        let page_addr = page_addr & 0xFFFFF000; //align addr to 4kb
        PageTableEntry(flags | page_addr)
    }

    fn set_page_addr(&mut self, page_addr: usize) {
        let page_addr = page_addr & 0xFFFFF000; //align addr to 4kb
        self.0 &= 0x00000FFF;
        self.0 |= page_addr;
    }

    fn set_present(&mut self, is_present: bool) {
        if is_present {
            self.0 |= 0x1;
        } else {
            self.0 &= 0xffff_fffe;
        }
    }
}

impl PageTable {
    fn contiguous(phys_addr: usize) -> Self {
        let mut pt = PageTable {
            entries: [PageTableEntry::new(0); 1024],
        };
        let mut offset = 0;
        for i in 0..1024 {
            pt.entries[i].set_page_addr(phys_addr + offset);
            pt.entries[i].set_present(true);
            offset += 0x1000;
        }
        pt
    }

    fn new() -> Self {
        let pt = PageTable {
            entries: [PageTableEntry::new(0); 1024],
        };
        pt
    }
}

impl PageDirectory {
    fn set_table(&mut self, virt_addr: usize, ptable: &PageTable) {
        let idx = virt_addr >> 22;
        self.entries[idx].set_table_addr((ptable as *const _) as usize);
        self.entries[idx].set_present(true);
    }
}

lazy_static! {
    static ref PAGE_DIR: UnintMutex<PageDirectory> = UnintMutex::new(PageDirectory {
        entries: [PageDirectoryEntry::new(0); 1024]
    });
    static ref PT_LIST: FixedPageTables = FixedPageTables {
        first_mb: PageTable::contiguous(0x0),
        stack: PageTable::contiguous(crate::STACK_ADDR),
        heap: PageTable::contiguous(crate::HEAP_ADDR)
    };
}

fn get_page_dir_phys_addr() -> u32 {
    ((&PAGE_DIR.lock().entries) as *const _) as u32
}

pub fn init() {
    // println!("{:x?} ", (&*PAGE_DIR) as *const _);
    // println!("{:x?} {:x?}", (&PT_LIST.first_mb) as *const _,
    //                          (&PT_LIST.stack) as *const _);

    
    PAGE_DIR.lock().set_table(0x0, &PT_LIST.first_mb);
    PAGE_DIR.lock().set_table(crate::STACK_ADDR, &PT_LIST.stack);
    PAGE_DIR.lock().set_table(crate::HEAP_ADDR, &PT_LIST.heap);

    let page_dir_addr = get_page_dir_phys_addr();
        
        //println!("{:x?}", PAGE_DIR.lock().entries[0]);

    without_interrupts(|| {
        //unsafe { println!("{:x?} {:x}", page_dir_addr, *(page_dir_addr as *const u32)); };
        enable_paging(page_dir_addr);
    });
   

    //println!("{:x?}", PT_LIST.stack.entries[1023]);
}

// Simple "filesystem"
// [ptr to table {4}] [data] [table num entries {4}] [TableEntries...] [total len - 4 {4}]
// TableEntry is defined below
// total len allows this to be written at the end of kernel.bin file

use std::fs;
use std::env;
use std::cmp::min;
use std::fs::File;
use std::io::{BufRead, BufReader};

#[derive(Debug, Copy, Clone)]
#[repr(C, packed)]
struct TableEntry {
    name: [u8; 28],
    addr: [u8; 4],
    len: [u8; 4]
}

fn main() -> std::io::Result<()> {
    let args: Vec<String> = env::args().collect();

    //return convert_palette_from_hex(&args[1], &args[2]);

    let res_folder = &args[1];
    let output = &args[2];

    let mut output_data: Vec<u8> = Vec::new();
    output_data.extend([0u8;4]); // table pointer

    let mut table: Vec<TableEntry> = Vec::new();
    for f in fs::read_dir(res_folder)? {
        let e = f?;
        if e.file_type()?.is_dir() {
            continue;
        }
        
        let path = e.path();
        let filename = path.file_name().unwrap().to_str().unwrap().as_bytes();
        let fnlen = min(filename.len(), 27);
        
        let data = fs::read(&path)?;
        let mut table_entry = TableEntry {
            name: [0u8; 28],
            addr: (output_data.len() as u32).to_le_bytes(),
            len: (data.len() as u32).to_le_bytes()
        };
        table_entry.name[0..fnlen].copy_from_slice(&filename[0..fnlen]);      
        
        table.push(table_entry);
        output_data.extend(data);        
    }

    let table_addr = output_data.len() as u32;
    output_data[0..4].copy_from_slice(&table_addr.to_le_bytes());
    
    output_data.extend((table.len() as u32).to_le_bytes());   
    for t in table {
        output_data.extend(t.name);
        output_data.extend(t.addr);
        output_data.extend(t.len);
    }

    output_data.extend((output_data.len() as u32).to_le_bytes());

    fs::write(output, output_data)?;

    Ok(())
}

// Some utilities
// TODO: support and move them out to different crate

// Convert palette from .hex files found on https://lospec.com to binary
// (u32 ARBG (little endian), A is always zero for every color in order)
fn convert_palette_from_hex(src_path: &str, dst_path: &str) -> std::io::Result<()> {
    let file = File::open(src_path)?;
    let reader = BufReader::new(file);
    let mut output: Vec<u8> = Vec::new();

    for line in reader.lines() {
        let line = line?;
        //println!("{}", line);       
        for i in 0..3 {
            output.push(u8::from_str_radix(&line[2*(2-i)..2*(2-i)+2], 16).unwrap());
        }
        output.push(0);
    }

    fs::write(dst_path, output)?;
    Ok(())
}

// // Read PSF1 file code
// use std::fs;
// use core::mem::size_of;

// #[derive(Debug)]
// struct PSF1Header {
//     magic: u16,        
//     mode: u8,
//     height: u8
// }

// fn main() {
//     if let Ok(r) = fs::read("/home/dzugaru/Documents/dzu-os/resources/FullCyrSlav-VGA8.psf") {
//         let header = unsafe { &*(r.as_ptr() as *const PSF1Header) };
//         println!("{:x?}", header);

//         let mut offset = size_of::<PSF1Header>();
//         for i in 0..128 {
//             println!("Glyph {}", i);
//             let glyph = &r[offset..offset + header.height as usize]; 
//             for row in glyph {
//                 let mut mask = 0b1000_0000;
//                 for _ in 0..8 {
//                     print!("{}", if mask & row > 0 { "0" } else { " " });
//                     mask >>= 1;
//                 }
//                 println!();
//             }
//             offset += header.height as usize;
//         }
//     }
// }
